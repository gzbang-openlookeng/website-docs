// 皮肤初始化
const themeStyle = localStorage.getItem("lookeng-theme");
const html = document.getElementsByTagName("html")[0];
if (!themeStyle) {
  localStorage.getItem("lookeng-theme", "light");
  html.classList.add("light");
} else {
  html.classList.add(themeStyle);
}
