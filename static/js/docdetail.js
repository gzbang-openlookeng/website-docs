// 页面语言、换肤及左侧导航栏相关脚本
$(function ($) {
  const lang = location.pathname.split("/")[1];
  var evaluateParams = {
    name: "",
    path: "",
    lang: "",
    version: "",
    stars: 0,
  };
  evaluateParams.lang = lang;
  if (lang === "zh") {
    $("#lang>.option>a").eq(0).addClass("action");
    $("#lang>.option>a").eq(1).removeClass("action");
    $("#lang>.option>a").eq(2).addClass("action");
    $("#lang>.option>a").eq(3).removeClass("action");
  } else {
    $("#lang>.option>a").eq(0).removeClass("action");
    $("#lang>.option>a").eq(1).addClass("action");
    $("#lang>.option>a").eq(2).removeClass("action");
    $("#lang>.option>a").eq(3).addClass("action");
  }
  // 切换语言
  $(".h5-right>.icon-lang,#lang").click(function (e) {
    $(this).find(".option").show();
    $(document).one("click", function () {
      $(this).find(".option").hide();
    });
    e.stopPropagation();
  });
  $(".right-content>#lang>.option>a,.h5-left>#lang>.option>a").click(
    function () {
      const url = location.href;
      if (url.search("/zh/") === -1) {
        if ($(this).attr("ref") === "zh") {
          window.location.replace(url.replace("/en/", "/zh/"));
        }
      } else {
        if ($(this).attr("ref") === "en") {
          window.location.replace(url.replace("/zh/", "/en/"));
        }
      }
    }
  );
});
// 左侧导航栏相关
function getTreeLink() {
  setTimeout(function () {
    let openEle = $("#docstreeview .jstree-container-ul").find(".jstree-open");
    for (let i = 0; i < openEle.length; i++) {
      if (i < openEle.length - 1) {
        let span = "<i></i>";
        $(".page-title")
          .append(
            $("#docstreeview .jstree-container-ul")
              .find(".jstree-open")
              .eq(i)
              .find("a")
              .first()
              .clone()
          )
          .append(span);
      } else {
        let text = $("#docstreeview .jstree-container-ul")
          .find(".jstree-open")
          .eq(i)
          .find("a")
          .first()
          .text();
        let span = "<span>" + text + "</span>";
        $(".page-title").append(span);
      }
    }
  }, 500);
}
getTreeLink();

setTimeout(function () {
  $("#docstreeview .jstree-container-ul a.active")
    .closest(".jstree-open")
    .children(".jstree-anchor")
    .find("p a")
    .css({
      color: "#FEB32A",
    });
}, 100);
// 皮肤主题切换
$(function ($) {
  const themeStyle = localStorage.getItem("lookeng-theme");
  const html = $("html");
  if (!themeStyle) {
    $(".theme-change i").removeClass("light dark").addClass("light");
    $(".title-h2 .icon-help").removeClass("dark");
    $(".nav-menu a .h5-logo").removeClass("dark");
    html.removeClass("light dark").addClass("light");
    localStorage.getItem("lookeng-theme", "light");
  } else {
    $(".theme-change i").removeClass("light dark").addClass(themeStyle);
    $(".title-h2 .icon-help").addClass(themeStyle);
    $(".nav-menu a .h5-logo").addClass(themeStyle);
    html.removeClass("light dark").addClass(themeStyle);
  }
  $(".theme-change i").click(function () {
    if ($(this).hasClass("light")) {
      $(".title-h2 .icon-help").addClass("dark");
      $(".nav-menu a .h5-logo").addClass("dark");
      $(this).addClass("dark").removeClass("light");
      localStorage.setItem("lookeng-theme", "dark");
      html.addClass("dark").removeClass("light");
    } else {
      $(".nav-menu a .h5-logo").removeClass("dark");
      $(".title-h2 .icon-help").removeClass("dark");
      $(this).addClass("light").removeClass("dark");
      localStorage.setItem("lookeng-theme", "light");
      html.addClass("light").removeClass("dark");
    }
  });
});

window.onload = function () {
  const lang = location.pathname.split("/")[1];
  if (lang === "zh") {
    let content = document.querySelector("#markdown");
    content.onclick = function (ev) {
      var ev2 = ev || window.event;
      ev2.cancelBubble = true;
    };
  }
};
