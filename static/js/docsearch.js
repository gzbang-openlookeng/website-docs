// 搜索相关
$(function ($) {
  const lang = location.pathname.split("/")[1];
  var searchMethods = {
    search: function (value, page, el) {
      let postData = {
        keyword: value,
        page: page,
        pageSize: 9,
        type: "docs",
        lang: lang,
      };
      let postCountData = {
        keyword: value,
        lang: lang,
      };
      $.ajax({
        type: "POST",
        url: "/search/count",
        data: JSON.stringify(postCountData),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (dataCount) {
          const dataArray = dataCount.obj.total;
          dataArray.forEach((item) => {
            if (item.key === "docs") {
              totalAmount = item.doc_count;
            }
          });
          $.ajax({
            type: "POST",
            url: "/search/docs",
            data: JSON.stringify(postData),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
              let dataArr = data.obj.records;
              if (page === 1) {
                let pag = new Pagination({
                  element: el,
                  type: 1,
                  pageIndex: 1,
                  pageSize: 10,
                  pageCount: pagecount,
                  total: totalAmount,
                  jumper: false,
                  singlePageHide: false,
                  prevText: "<",
                  nextText: ">",
                  disabled: false,
                  currentChange: function (index) {
                    searchMethods.search(decodeURI(keyword), index);
                  },
                });
                $(".search-result>.title")
                  .find(".res-amount")
                  .text(totalAmount);
                $(".search-result>.title").find(".keyword").text(value);
              }
              searchMethods.solveData(dataArr, versionText);
            },
            error: function () {
              totalAmount = 0;
              $(".search-result>.title").find(".res-amount").text(totalAmount);
              $(".search-result>.title").find(".keyword").text(value);
            },
          });
        },
      });
    },
    solveData: function (result) {
      $(".search-result>ul").empty();
      if (!result.length) {
        $("#search_content").hide();
        return;
      }
      $("#search_content").show();
      result.forEach(function (item, index) {
        let url = "/" + item.path.replace("/docs/", "/docs/docs/") + ".html";
        $(".search-result>ul").append(
          `<li class=${index + 1}>` +
            '<div class="res-title" href="' +
            searchMethods.escapeHTML(url) +
            '">' +
            item.title +
            "</div>" +
            '<div class="res-desc">' +
            item.textContent +
            "</div>" +
            "</li>"
        );

        $(".search-result>ul li")
          .find(".res-title")
          .click(function () {
            window.location.href = $(this).attr("href");
          });
      });
    },
    escapeHTML: function (str) {
      return str.replace(/[&<>'"]/g, function (tag) {
        return (
          {
            "&": "&amp;",
            "<": "&lt;",
            ">": "&gt;",
            "'": "&#39;",
            '"': "&quot;",
          }[tag] || tag
        );
      });
    },
  };
  // 移动端左上角菜单图标点击事件
  $(".h5-head>.menu>.menu-open").click(function () {
    $(".h5-head>.menu>.menu-open").css("display", "none");
    $(".h5-head>.menu>.menu-close").css("display", "block");
    $(".h5-left").css("display", "flex");
    $("#menu-box").css("display", "block");
    $(".mask-nav").css("display", "block");
  });
  $(".h5-head>.menu>.menu-close").click(function () {
    $(".h5-head>.menu>.menu-close").css("display", "none");
    $(".h5-head>.menu>.menu-open").css("display", "block");
    $(".h5-left").css("display", "none");
    $("#menu-box").css("display", "none");
    $(".mask-nav").css("display", "none");
  });
  //左侧导航栏遮罩层点击事件
  $(".mask-nav").click(function () {
    $(".h5-head>.menu>.menu-close").css("display", "none");
    $(".h5-head>.menu>.menu-open").css("display", "block");
    $(".h5-left").css("display", "none");
    $("#menu-box").css("display", "none");
    $(".mask-nav").css("display", "none");
  });

  var keyword = "";
  $(".baseof_mask").click(function () {
    $(".baseof_mask").css("display", "none");
    $(".alert").css("display", "none");
    $("#result-container").css("display", "none");
  });

  $(".search-header>.icon-search").click(function () {
    keyword = $(".search-header>.search-text").val();
    $("#search_content").toggleClass();
    searchMethods.search(decodeURI(keyword), 1, "#baseof-pagination");
  });

  $(".search-header>.search-text").bind("change", function (event) {
    keyword = $(".search-header>.search-text").val();
    $("#search_content").css("display", "block");
    searchMethods.search(decodeURI(keyword), 1, "#baseof-pagination");
  });

  $(document).ready(function () {
    $(".search-header>.search-text").blur(function () {
      var value = $(this).val();
      value = $.trim(value);
      if (value == "") {
        searchMethods.search(decodeURI(value), 1, "#baseof-pagination");
        $(".search-result>#baseof-pagination").css("display", "none");
      }
    });
  });

  $("#search-input>.icon-search").click(function () {
    keyword = $("#search-input>.search-text").val();
    $("#search_content").css("display", "block");
    searchMethods.search(decodeURI(keyword), 1, "#web-pagination");
  });

  $("#search-input>.search-text").bind("keyup", function (event) {
    if (!$("#search-input>.search-text").val()) {
      $("#search_content").css("display", "none");
    }
    if (event.keyCode == "13") {
      keyword = $("#search-input>.search-text").val();
      $("#search_content").css("display", "block");
      searchMethods.search(decodeURI(keyword), 1, "#web-pagination");
    }
  });

  $(".white_search").click(function () {
    $(".searcher").css("display", "block");
    $(".zhezhao").css("display", "block");
    var height = $(".h5_index").outerHeight(true) - 279;
    $(".zhezhao").css("height", height);
  });

  $(".zhezhao").click(function () {
    $(".searcher").css("display", "none");
    $(".zhezhao").css("display", "none");
  });

  $(".h5-search")
    .find(".search-btn")
    .click(function () {
      keyword = $(".h5-search").find("input").val();
      $(".search-result").css("display", "block");
      searchMethods.search(decodeURI(keyword), 1, "#pagination");
    });

  $(".h5-search>.search-text").bind("keypress", function (event) {
    if (event.keyCode == "13") {
      keyword = $(".h5-search>.search-text").val();

      $(".search-result").css("display", "block");
      searchMethods.search(decodeURI(keyword), 1, "#pagination");
    }
  });
  var versionText = "";
  var totalAmount = 0;
  var pagecount = 5;
  var currentScreen = document.body.clientWidth;
  if (currentScreen <= 1000) {
    pagecount = 3;
  }
  if (lang === "zh") {
    versionText = "版本";
  } else if (lang === "en") {
    versionText = "version";
  } else {
    versionText = "Версия";
  }
  if (typeof keyword === "undefined") {
    $(".search-result").empty();
  } else {
    $(".search-result>.title").find(".keyword").text(decodeURI(keyword));
    $(".input>.search-text").val(decodeURI(keyword));
    $(".h5-search>div").find("input").val(decodeURI(keyword));
  }
});
