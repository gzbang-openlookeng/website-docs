FROM ubuntu:16.04 as builder

RUN apt -y update &&apt -y install curl
COPY . /src/website/
ENV HUGO_VERSION=0.104.3

RUN mkdir -p /usr/local/src && \
  cd /usr/local/src && \
  curl -L https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_extended_${HUGO_VERSION}_Linux-64bit.tar.gz | tar -xz && \
  mv hugo /usr/local/bin/
RUN cd /src/website/ && hugo -b / --minify

FROM swr.cn-north-4.myhuaweicloud.com/opensourceway/openeuler/nginx:1.22.0-22.03-lts

COPY --from=builder /src/website/public/ /usr/share/nginx/html/
RUN chown nginx:nginx -R /usr/share/nginx/html
COPY ./deploy/nginx.conf /etc/nginx/nginx.conf

RUN touch /var/run/nginx.pid \
    && chown -R nginx:nginx /var/log/nginx \
    && chown -R nginx:nginx /var/run/nginx.pid \
    && chown -R nginx:nginx /etc/nginx

EXPOSE 8080

USER nginx

ENTRYPOINT ["nginx", "-g", "daemon off;"]
